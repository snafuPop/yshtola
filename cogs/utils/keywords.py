from enum import Enum

class Keyword(Enum):
	LEFT = "<:left:1011270488326144020>"
	RIGHT = "<:right:1011270489206952036>"
	SUCCESS = ":white_check_mark:"
	YES = "<:yes:603662870567190597>"
	NO = "<:no:603662870365995019>"
	ERROR = ":no_entry:"
	LOAD = ":record_button:"
	UNLOAD = ":eject:"
	RELOAD = ":repeat:"
	BULLET = "▪️"
	BOOST = "<:boost:907765841771257928>"
	CURRENCY = "<a:goldbig:888588312254746654>"
	NAMETAG = "<:nametag:1009939862180352000>"
